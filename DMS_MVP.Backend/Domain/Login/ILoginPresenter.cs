﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS_MVP.Backend.Domain.Login
{
    public interface ILoginPresenter
    {
        Task Authenticate(string operatorName, string password);
    }
}
