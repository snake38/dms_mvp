﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using DMS_MVP.Backend.Domain.Login.Events;

namespace DMS_MVP.Backend.Domain.Login
{
    public class LoginPresenter : ILoginPresenter, OnGetDmsOperators
    {
        private ILoginView view;

        public LoginPresenter(ILoginView view)
        {
            this.view = view;

            GetDmsOperators(this);
        }

        public async Task Authenticate(string operatorName, string password)
        {
            await Task.Delay(4000);

            if (operatorName.Length > 3 && password.Length > 3)
            {
                view?.OpenMainApplication();
            }
            else
            {
                view?.LoginError("Operator lub hasło są błędne!");
            }
        }

        public async void GetDmsOperators(OnGetDmsOperators onDmsOperators)
        {
            using (var conn = new System.Data.SqlClient.SqlConnection("Server=localhost;Database=SORDREW;User Id=sa;Password=connecto$2015;"))
            {
                var operators = await conn.QueryAsync<string>(@"
                    SELECT RTRIM(LTRIM(Ope_Imie + ' ' + Ope_Nazwisko)) AS NameAndSurname 
                    FROM CNT.[sys.Operators] 
                    ORDER BY NameAndSurname");

                await Task.Delay(2500);

                onDmsOperators?.OnGetDmsOperators(operators);
            }
        }

        public void OnGetDmsOperators(IEnumerable<string> operators)
        {
            view?.SetOperators(operators);
        }
    }
}
