﻿using DMS_MVP.WinForms.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DMS_MVP.WinForms
{
    static class Program
    {
        public static bool OpenMainApp = false;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            new LoginForm();

            if (OpenMainApp)
            {
                new MainForm();
            }
        }
    }
}
