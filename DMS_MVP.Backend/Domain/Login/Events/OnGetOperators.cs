﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS_MVP.Backend.Domain.Login.Events
{
    public interface OnGetDmsOperators
    {
        void OnGetDmsOperators(IEnumerable<string> operators);
    }
}
