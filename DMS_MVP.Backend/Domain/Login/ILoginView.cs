﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS_MVP.Backend.Domain.Login
{
    public interface ILoginView
    {
        void SetOperators(IEnumerable<string> operatorsName);

        void LoginError(string message);

        void OpenMainApplication();
    }
}
