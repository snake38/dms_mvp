﻿using DMS_MVP.Backend.Domain.Login;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DMS_MVP.WinForms.Forms
{
    public partial class LoginForm : Form, ILoginView
    {
        private ILoginPresenter presenter;

        public LoginForm()
        {
            InitializeComponent();

            presenter = new LoginPresenter(this);

            Application.Run(this);
        }

        public void LoginError(string message)
        {
            lblStatus.Text = message;
        }

        public void OpenMainApplication()
        {
            Program.OpenMainApp = true;
            Close();
        }

        public void SetOperators(IEnumerable<string> operatorsName)
        {
            cbOperators.Items.Clear();
            foreach (var operatorName in operatorsName)
            {
                cbOperators.Items.Add(operatorName);
            }
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            presenter.Authenticate(cbOperators.Text, txtPassword.Text);
        }
    }
}
